import React, { Component } from 'react';
import './App.css';

class App extends React.Component
{
  constructor(props)
  {
    super(props)
    this.state = {
      title: 'React JS CRUD',
      act: 0,
      index: '',
      datas: []
    }
  }

  componentDidMount()
  {
    this.refs.name.focus();
  }

  fSubmit = (e) =>
  {
    e.preventDefault();

    let datas = this.state.datas;
    let name = this.refs.name.value;
    let address = this.refs.address.value;
    
    if(this.state.act === 0) //new data
    {
      let data = {
        name, address
      }

      datas.push(data);
    }
    else //update data
    {
      let index = this.state.index;
      datas[index].name = name
      datas[index].address = address
    }

    this.setState({
      datas: datas,
      act: 0
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  }

  fRemove = (i) =>
  {
    let datas = this.state.datas;
    datas.splice(i,1)
    this.setState(
      {
        datas: datas
      }
    )
  }

  fEdit = (i) =>
  {
    let data = this.state.datas[i]
    this.refs.name.value = data.name;
    this.refs.address.value = data.address;

    this.setState(
      {
        act: 1,
        index: i
      }
    )

    this.refs.name.focus()
  }

  render()
  {
    let datas = this.state.datas;
    return (
      <div className="App">
        <h2>{this.state.title}</h2>
        <form ref="myForm" className="myForm">
          <input type="text" ref="name" placeholder="Your Name" className="formField"></input>
          <input type="address" ref="address" placeholder="Your Address" className="formField"></input>
          <button onClick={(e) =>this.fSubmit(e)} className="myButton">SUBMIT</button>
        </form>
        <pre>
          <br/>
          <table>
            <tr>
              <td>No.</td>
              <td>Name</td>
              <td>Address</td>
              <td>Action </td>
            </tr>
          {datas.map((data, i) =>
            <tr key={i} className="myList">
              <td>{i+1}</td> 
              <td>{data.name}</td> 
              <td> {data.address}</td>
              <td><button onClick={() =>this.fRemove(i)} className="delBtn">DELETE</button>
              <button onClick={() =>this.fEdit(i)} className="editBtn">EDIT</button></td>
            </tr>
          )}
          </table>
        </pre>
      </div>
    );
  }
}

export default App;
